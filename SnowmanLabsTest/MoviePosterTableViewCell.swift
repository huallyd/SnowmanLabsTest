//
//  MoviePosterTableViewCell.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class MoviePosterTableViewCell: UITableViewCell {
    
    static var identifier: String {
        return "posterCell"
    }
    
    static var nibName: String {
        return "MoviePosterTableViewCell"
    }
    
    static var cellHeight: CGFloat {
        return 100.0
    }

    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureButton()
    }
    
    func configureButton(){
        likeButton.setImage(#imageLiteral(resourceName: "like_button"), for: .normal)
        likeButton.setImage(#imageLiteral(resourceName: "unlike_button"), for: .selected)
    }
    
    @IBAction func likeMovie(_ sender: Any) {
        likeButton.isSelected = !likeButton.isSelected
    }
    
}

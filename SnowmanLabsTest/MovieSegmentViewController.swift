//
//  MovieSegmentViewController.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

protocol SegmentControlPageDelegate {
    func segmentScrolled(_ viewIndex: Int)
}

protocol RequestMovieDelegate {
    func fetchMoreMovies(type: FilterMovieType)
}

class MovieSegmentViewController: UIViewController {
    
    var previousPage: Int = 0
    var segmentControlPageDelegate: SegmentControlPageDelegate?
    var presenter: MoviePresenter = MoviePresenter()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLayoutSubviews() {
        scrollView.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setViewDelegate(view: self)
        getMovies()
    }
    
    func getMovies(){
        presenter.getMovies(type: .popular)
        presenter.getMovies(type: .topRated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? PopularMovieViewController {
            controller.requestMovieDelegate = self
        }
        
        if let controller = segue.destination as? EvaluatedMovieViewController {
            controller.requestMovieDelegate = self
        }
    }
    
}

// MARK: - UIScrollViewDelegate
extension MovieSegmentViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.setupPage()
    }
    func setupPage(){
        let pageWidth: CGFloat = scrollView.frame.size.width
        let fractionalPage: CGFloat = scrollView.contentOffset.x / pageWidth
        let page: Int = lround(Double(fractionalPage))
        if previousPage != page {
            segmentControlPageDelegate?.segmentScrolled(page)
            previousPage = page
        }
    }
}

// MARK: - SegmentControlButtonDelegate

extension MovieSegmentViewController: SegmentControlButtonDelegate {
    func segmentSelected(_ viewIndex: Int) {
        var rectToScroll = self.view.frame
        rectToScroll.origin.x = self.view.frame.width * CGFloat(viewIndex)
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: UIViewAnimationOptions(), animations: {
            self.scrollView.scrollRectToVisible(rectToScroll, animated: false)
        }, completion: nil)
    }
}


// MARK: - Presenter protocol

extension MovieSegmentViewController: MovieDelegate {
    func reload(filterMovie: FilterMovieType) {
        switch filterMovie {
        case .popular:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationKeys.updatePopularMovies), object: presenter.popularMovies, userInfo: nil)
        case .topRated:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationKeys.updateEvaluatedMovies), object: presenter.topMovies, userInfo: nil)
        default:
            break
        }
    }

    func showMessage(title: String, msg: String) {
         present(ViewUtil.alertControllerWithTitle(title: title, withMessage: msg), animated: true, completion: nil)
    }
}

extension MovieSegmentViewController: RequestMovieDelegate {
    
    func fetchMoreMovies(type: FilterMovieType) {
        switch type {
        case .popular:
            presenter.getMovies(type: .popular)
        default:
            presenter.getMovies(type: .topRated)
        }
    }

    
}

//
//  User.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class User: Object {
    
    dynamic var id: Int = 0
    let favoriteMovies = List<Movie>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    convenience required init (id: Int) {
        self.init()
        self.id = id
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
    required init() {
        super.init()
    }
    
    private func getMovie(of id: Int) -> Movie? {
        var movies = [Movie]()
        
        movies = favoriteMovies.filter() {
            $0.id == id
        }
        
        if !movies.isEmpty {
            return movies.first
        } else {
            return nil
        }
    }
    
    func movieIsFavorite (id: Int) -> Bool? {
        if let _ = getMovie(of: id) {
           return true
        } else {
            return false
        }
    }
    

    func createFavoriteMovie(movie: Movie) {
        try! Realm().write {
            self.favoriteMovies.append(movie)
            try! Realm().add(movie)
            try! Realm().add(self, update: true)
        }
    }
    
    private func removeMovieById(id: Int) {
        var newFavoriteMovies = [Movie]()
        for movie in favoriteMovies where movie.id != id {
            newFavoriteMovies.append(movie)
        }
        try! Realm().write {
            favoriteMovies.removeAll()
            newFavoriteMovies.forEach {
                favoriteMovies.append($0)
            }
        }
    }

    func removeFavoriteMovie(movie: Movie) {
        if let movieDeleted = getMovie(of: movie.id) {
            removeMovieById(id: movieDeleted.id)
            try! Realm().write {
                try! Realm().add(self, update: true)
            }
        }
    }
    
    func getFavoriteMovies() -> [Movie] {
        let movies: [Movie] = Array(favoriteMovies)
        return movies
    }
}



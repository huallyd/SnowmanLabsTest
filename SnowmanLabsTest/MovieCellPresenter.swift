//
//  MovieCellPresenter.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import SDWebImage

class MovieCellPresenter: NSObject {
    
    var movie: Movie?
    let widthImage: Int = 500
    
    func getPosterOfMovie(imageView: UIImageView){
        guard let imageData = movie?.image else {
            let url = UrlServer.serverImage + "w\(widthImage)" +  movie!.posterPath! + "?" + UrlServer.token.stringFromHttpParameters()
            
            imageView.setIndicatorStyle(.gray)
            imageView.setShowActivityIndicator(true)
            imageView.sd_setImage(with: URL(string: url), completed: { (image, error, type, url) in
                if error == nil {
                    self.movie?.image = UIImagePNGRepresentation(image!)
                }
            })
            return
        }
        imageView.image = UIImage(data: imageData)
    }
}

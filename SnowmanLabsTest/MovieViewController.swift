//
//  MovieViewController.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 07/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit


protocol SegmentControlButtonDelegate {
    func segmentSelected(_ viewIndex: Int)
}

class MovieViewController: UIViewController {
    
    var segmentSelected: Int? {
        didSet {
            setFontButtonBySegmentSelected()
        }
    }
    
    var segmentControlButtonDelegate: SegmentControlButtonDelegate?
    
    @IBOutlet weak var popularButton: UIButton!
    @IBOutlet weak var evaluatedButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.segmentSelected = 0
    }
    
    func setFontButtonBySegmentSelected(){
        let buttons = [popularButton, evaluatedButton, favoriteButton]
        
        for (i, button) in buttons.enumerated() {
            if i == segmentSelected {
                button!.alpha = 1.0
            } else {
                button!.alpha = 0.5
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segmentVC = segue.destination as? MovieSegmentViewController {
            segmentControlButtonDelegate = segmentVC
            segmentVC.segmentControlPageDelegate = self
        }
    }
    
    @IBAction func showPopularMovies(_ sender: Any? = nil) {
        segmentControlButtonDelegate?.segmentSelected(0)
    }
    
    @IBAction func showEvaluateMovies(_ sender: Any? = nil) {
        segmentControlButtonDelegate?.segmentSelected(1)
    }
    
    @IBAction func showFavoriteMovies(_ sender: Any? = nil) {
        segmentControlButtonDelegate?.segmentSelected(2)
    }
}

// MARK: - SegmentControlPageDelegate

extension MovieViewController: SegmentControlPageDelegate {
    func segmentScrolled(_ viewIndex: Int) {
        switch viewIndex {
        case 0:
            showPopularMovies()
            segmentSelected = 0
        case 1:
            showEvaluateMovies()
            segmentSelected = 1
        case 2:
            showFavoriteMovies()
            segmentSelected = 2
        default: break
            
        }
    }
}

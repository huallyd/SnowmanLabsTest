//
//  ViewUtil.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 09/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class ViewUtil: NSObject {
    static func alertControllerWithTitle(title: String, withMessage message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        return alert
    }
}

extension UIView {
    static func heightScaleProportion() -> CGFloat {
        return UIScreen.main.bounds.height / 667.0
    }
    
    static func widthScaleProportion() -> CGFloat {
        return UIScreen.main.bounds.width / 375.0
    }
}

extension UILabel {
    func setDynamicFont() {
        self.font = self.font!.withSize(UIView.heightScaleProportion()*self.font!.pointSize)
    }
}

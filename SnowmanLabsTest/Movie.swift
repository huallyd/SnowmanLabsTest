//
//  Movie.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 07/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import ObjectMapper
import Realm
import RealmSwift

class Movie: Object, Mappable {
    
    dynamic var id: Int = 0
    dynamic var title: String?
    dynamic var voteAverage: Double = 0
    dynamic var posterPath: String?
    dynamic var overview: String?
    dynamic var releaseDate: String?
    dynamic var image: Data?


    required init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
       self.init()
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    required init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }

    func mapping(map: Map) {
        id <- map[MovieUrlKeys.id]
        title <- map[MovieUrlKeys.title]
        posterPath <- map[MovieUrlKeys.posterPath]
        overview <- map[MovieUrlKeys.overview]
        voteAverage <- map[MovieUrlKeys.voteAverage]
        releaseDate <- map[MovieUrlKeys.releaseDate]
    }
    
}

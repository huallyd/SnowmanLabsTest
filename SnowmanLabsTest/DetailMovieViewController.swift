//
//  DetailMovieViewController.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class DetailMovieViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var presenter = DetailMoviePresenter()

    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
        configureTableView()
    }
    
    func configureTableView() {
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    func registerNib() {
        tableView.register(UINib(nibName: MoviePosterTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: MoviePosterTableViewCell.identifier)
        tableView.register(UINib(nibName: MovieInformationsTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: MovieInformationsTableViewCell.identifier)
    }
    
    func updateFavoriteMovies() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationKeys.updateFavoriteMovies), object: nil, userInfo: nil)
    }
    
    func addFavoriteMovies(_ sender: UIButton) {
        presenter.addFavoriteMovie()
        updateFavoriteMovies()
    }
    
    func generatePosterCell (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: MoviePosterTableViewCell.identifier, for: indexPath) as! MoviePosterTableViewCell
        
        cell.posterImage.image = UIImage(data: presenter.movie!.image!)
        cell.likeButton.addTarget(self, action: #selector(addFavoriteMovies), for: .touchUpInside)
        cell.likeButton.isSelected = presenter.user.movieIsFavorite(id: presenter.movie!.id)!
        
        return cell
    }
    
    
    func generateInformationsCell (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieInformationsTableViewCell.identifier, for: indexPath) as! MovieInformationsTableViewCell
        
        cell.movie = presenter.movie
        
        return cell
    }
}

extension DetailMovieViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0: return generatePosterCell(tableView, cellForRowAt: indexPath)
        case 1: return generateInformationsCell(tableView, cellForRowAt: indexPath)
        default: return UITableViewCell()
        }
    }
    
}

extension DetailMovieViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
}

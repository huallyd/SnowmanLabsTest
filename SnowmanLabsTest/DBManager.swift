//
//  DBManager.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class DBManager: NSObject {
    
    static func addObjc(_ obj: Object){
        try! Realm().write(){
            try! Realm().add(obj)
        }
    }
    
    static func getAll<T : Object>() -> [T] {
        let objs: Results<T> = {
            try! Realm().objects(T.self)
        }()
        
        return Array(objs)
    }
}

//
//  Constants.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 07/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//


//MARK - URL Server

struct UrlServer {
    static let token = ["api_key": "5974484d1cdc7474135e04c6c8902486"]
    static let server = "http://api.themoviedb.org/3/movie/"
    static let popularMovies = "\(server)popular"
    static let topRated = "\(server)top_rated"
    static let serverImage = "https://image.tmdb.org/t/p/"
}

//MARK - Movie URL keys

struct MovieUrlKeys {
    static let results = "results"
    static let page = "page"
    static let id = "id"
    static let title = "title"
    static let originalTitle = "original_title"
    static let backdropPath = "backdrop_path"
    static let voteAverage = "vote_average"
    static let releaseDate = "release_date"
    static let originalLanguage = "original_language"
    static let voteCount = "vote_count"
    static let posterPath = "poster_path"
    static let overview = "overview"
    static let totalPages = "total_pages"
    static let popularity = "popularity"
    static let video = "video"
    static let genre_ids = "genre_ids"
    static let adult = "adult"
}

//MARK - Segue Identifiers

struct SegueIdentifiers {
    static let listMovieToDetailMovie = "listMovieToDetailMovie"
}

//MARK - Notification Keys

struct NotificationKeys {
    static let updatePopularMovies = "updatePopularMovies"
    static let updateEvaluatedMovies = "updateEvaluatedMovies"
    static let updateFavoriteMovies = "updateFavoriteMovies"
}


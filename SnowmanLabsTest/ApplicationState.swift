//
//  ApplicationState.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class ApplicationState: NSObject {
    
    var currentUser: User?
    
    static let sharedInstance : ApplicationState = {
        let instance = ApplicationState(singleton: true)
        return instance
    }()
    
    private init(singleton: Bool) {
        super.init()
        self.currentUser = DBManager.getAll().first
    }
}

//
//  FavoriteMovieViewController.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class FavoriteMovieViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter = MovieFilterPresenter()
    var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
        registerObservers()
    }
    
    func registerNib(){
        let nib = UINib(nibName: MovieCollectionViewCell.nibName, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: MovieCollectionViewCell.identifier)
    }
    
    func registerObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(updateMovies(_:)), name: NSNotification.Name(rawValue: NotificationKeys.updateFavoriteMovies), object: nil)
    }
    
    func updateMovies (_ notification: Notification) {
        collectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? DetailMovieViewController {
            controller.presenter.movie = presenter.getFavoriteMoviesOfUser()[selectedIndexPath!.item]
        }
    }
    
}

extension FavoriteMovieViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.identifier, for: indexPath) as! MovieCollectionViewCell
        
        cell.presenter.movie = presenter.getFavoriteMoviesOfUser()[indexPath.row]
        cell.updateCell()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return presenter.getFavoriteMoviesOfUser().count
    }
}

extension FavoriteMovieViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndexPath = indexPath
        
        performSegue(withIdentifier: SegueIdentifiers.listMovieToDetailMovie, sender: self)
        
    }
}

extension FavoriteMovieViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: MovieCollectionViewCell.cellSize.width * UIView.widthScaleProportion(), height: MovieCollectionViewCell.cellSize.height*UIView.heightScaleProportion())
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0.1
    }
}

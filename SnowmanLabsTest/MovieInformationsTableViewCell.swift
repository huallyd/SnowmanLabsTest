//
//  MovieInformationsTableViewCell.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class MovieInformationsTableViewCell: UITableViewCell {

    static var identifier: String {
        return "movieInformationsCell"
    }
    
    static var nibName: String {
        return "MovieInformationsTableViewCell"
    }
    
    static var cellHeight: CGFloat {
        return 100.0
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseLabel: UILabel!
    @IBOutlet weak var evaluationLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    var movie: Movie? {
        didSet{
            updateCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupLabelsSize()
    }
    
    func setupLabelsSize() {
        let labels = [titleLabel, releaseLabel, evaluationLabel, overviewLabel]
        labels.forEach {
            $0?.setDynamicFont()
        }
    }

    func updateCell(){
        titleLabel.text = movie!.title
        evaluationLabel.text = "Avaliação: \(movie!.voteAverage.description)"
        releaseLabel.text = "Lançamento: \(movie!.releaseDate!)"
        overviewLabel.text = movie!.overview
    }
}

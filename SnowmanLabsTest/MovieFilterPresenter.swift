//
//  MovieFilterPresenter.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

protocol MovieFilterDelegate {
    func reload()
    func showMessage(title: String, msg: String)
}

class MovieFilterPresenter: NSObject {
    
    var movies: [Movie] = [Movie]()
    var user: User = ApplicationState.sharedInstance.currentUser!
    var view: MovieFilterDelegate?
    
    
    func getFavoriteMoviesOfUser() -> [Movie] {
        return user.getFavoriteMovies()
    }

}

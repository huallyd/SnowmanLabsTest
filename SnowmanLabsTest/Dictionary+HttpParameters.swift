//
//  Dictionary+HttpParameters.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import Foundation

extension Dictionary {
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).stringByAddingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).stringByAddingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joined(separator: "&")
    }
    
}

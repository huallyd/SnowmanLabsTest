//
//  MovieRequest.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 07/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class MovieRequest: NSObject {
    
    static func getMovies(type: FilterMovieType, page: Int, completionHandler: @escaping (_ success: Bool, _ msg: String, _ movies: [Movie]?) -> ()) {
    
        var movies = [Movie]()
        var params = [String: Any]()
        params[MovieUrlKeys.page] = page
        params[UrlServer.token.keys.first!] = UrlServer.token.values.first
        
        let url = type == .popular ? UrlServer.popularMovies : UrlServer.topRated
        
        Alamofire.request(url, method: .get, parameters: params).responseJSON { (response: DataResponse<Any>) in
            switch response.result {
                
            case .success:
                let data = response.result.value as! Dictionary<String, AnyObject>
                switch response.response!.statusCode {
                case 200:
                    if let results = data[MovieUrlKeys.results] as? Array<[String:AnyObject]> {
                        for result in results {
                            if let movie = Mapper<Movie>().map(JSON: result){
                                movies.append(movie)
                            }
                        }
                    }
                    completionHandler(true, "success", movies)
                    
                default:
                    completionHandler(false, "fail", nil)
                }
                
            case .failure(_):
                completionHandler(false, "network error", nil)
            }
        }
    }
}

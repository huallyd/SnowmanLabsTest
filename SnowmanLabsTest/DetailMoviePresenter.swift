//
//  DetailMoviePresenter.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

protocol DetailMovieDelegate {
    func reload(filterMovie: FilterMovieType)
    func showMessage(title: String, msg: String)
}

class DetailMoviePresenter: NSObject {
    let pagination = 3
    var movie: Movie?
    var view: DetailMovieDelegate?
    var user: User = ApplicationState.sharedInstance.currentUser!

    
    func setViewDelegate(view: DetailMovieDelegate) {
        self.view = view
    }
    
    func addFavoriteMovie() {
        if !user.movieIsFavorite(id: movie!.id)! {
            user.createFavoriteMovie(movie: movie!)
        } else {
            user.removeFavoriteMovie(movie: movie!)
        }
    }
}

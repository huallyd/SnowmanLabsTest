//
//  MoviePresenter.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

enum FilterMovieType {
    case favorite
    case topRated
    case popular
}

enum OperatorType {
    case add
    case decrease
}

protocol MovieDelegate {
    func reload(filterMovie: FilterMovieType)
    func showMessage(title: String, msg: String)
}

class MoviePresenter: NSObject {
    var popularMovies: [Movie] = [Movie]()
    var topMovies: [Movie] = [Movie]()
    var view: MovieDelegate?
    fileprivate var popularPage = 1
    fileprivate var topPage = 1
    
    func setViewDelegate(view: MovieDelegate) {
        self.view = view
    }
    
    func getMovies(type: FilterMovieType) {
        let page = type == .popular ? popularPage : topPage
        MovieRequest.getMovies(type: type, page: page) { (success, msg, movies) in
            if success {
                self.appendMoviesInTypeFilter(movies: movies!, type: type)
                self.view?.reload(filterMovie: type)
            } else {
                self.updateCount(type: type, operatorType: .decrease)
                self.view?.showMessage(title: "Error", msg: msg)
            }
        }
        updateCount(type: type, operatorType: .add)
    }
    
    func updateCount(type: FilterMovieType, operatorType: OperatorType) {
        switch type {
        case .popular:
            if operatorType == .add {
                popularPage = popularPage+1
            } else {
                popularPage = topPage-1
            }
        default:
            if operatorType == .add {
                topPage = topPage+1
            } else {
                topPage = topPage-1
            }
        }
    }
    
    func appendMoviesInTypeFilter(movies: [Movie], type: FilterMovieType) {
        switch type {
        case .popular:
            movies.forEach {
                self.popularMovies.append($0)
            }
        default:
            movies.forEach {
                self.topMovies.append($0)
            }
        }
    }
}

//
//  MovieCollectionViewCell.swift
//  SnowmanLabsTest
//
//  Created by Huallyd Smadi on 08/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    var presenter: MovieCellPresenter = MovieCellPresenter()
    
    @IBOutlet weak var posterImage: UIImageView!
    
    static var identifier: String {
        return "movieCell"
    }
    
    static var nibName: String {
        return "MovieCollectionViewCell"
    }
    
    static var cellSize: CGSize {
        return CGSize(width: 120, height: 200)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }
    func updateCell(){
        presenter.getPosterOfMovie(imageView: posterImage)
    }
}
